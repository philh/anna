# THIS FILE IS GENERATED AUTOMATICALLY FROM THE D-I PO MASTER FILES
# The master files can be found under packages/po/
#
# DO NOT MODIFY THIS FILE DIRECTLY: SUCH CHANGES WILL BE LOST
#
# translation of uk.po to Ukrainian
# translation of uk.po to
# Ukrainian messages for debian-installer.
# Copyright (C) 2003 Software in the Public Interest, Inc.
# This file is distributed under the same license as debian-installer.
#
#
# Translations from iso-codes:
# Eugeniy Meshcheryakov <eugen@debian.org>, 2005, 2006, 2007, 2010.
# Євгеній Мещеряков <eugen@debian.org>, 2008.
# Borys Yanovych <borys@yanovy.ch>, 2010, 2011.
# Maxim V. Dziumanenko <dziumanenko@gmail.com>, 2010.
# Yuri Chornoivan <yurchor@ukr.net>, 2010, 2011, 2012, 2013.
# Anton Gladky <gladk@debian.org>, 2011-2016
msgid ""
msgstr ""
"Project-Id-Version: uk\n"
"Report-Msgid-Bugs-To: anna@packages.debian.org\n"
"POT-Creation-Date: 2019-11-05 23:57+0100\n"
"PO-Revision-Date: 2019-03-20 22:31+0100\n"
"Last-Translator: Anton Gladky <gladk@debian.org>\n"
"Language-Team: Ukrainian <translation-team-uk@lists.sourceforge.net>\n"
"Language: uk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n"
"%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);\n"

#. Type: multiselect
#. Description
#. :sl2:
#. Type: multiselect
#. Description
#. :sl2:
#: ../anna.templates:1001 ../anna.templates:2001
msgid "Installer components to load:"
msgstr "Компоненти встановлювача для завантаження:"

#. Type: multiselect
#. Description
#. :sl2:
#: ../anna.templates:1001
msgid ""
"All components of the installer needed to complete the install will be "
"loaded automatically and are not listed here. Some other (optional) "
"installer components are shown below. They are probably not necessary, but "
"may be interesting to some users."
msgstr ""
"Всі компоненти встановлювача, необхідні для завершення встановлення, буде "
"завантажено автоматично і вони тут не показані. Деякі інші (необов'язкові) "
"компоненти наведено нижче. Вони, мабуть, не є необхідними, хоча й можуть "
"зацікавити деяких користувачів."

#. Type: multiselect
#. Description
#. :sl2:
#. Type: multiselect
#. Description
#. :sl2:
#: ../anna.templates:1001 ../anna.templates:2001
msgid ""
"Note that if you select a component that requires others, those components "
"will also be loaded."
msgstr ""
"Зауважте, що якщо ви виберете компонент, для якого потрібні інші компоненти, "
"то вони також будуть встановлені."

#. Type: multiselect
#. Description
#. :sl2:
#: ../anna.templates:2001
msgid ""
"To save memory, only components that are certainly needed for an install are "
"selected by default. The other installer components are not all necessary "
"for a basic install, but you may need some of them, especially certain "
"kernel modules, so look through the list carefully and select the components "
"you need."
msgstr ""
"Щоб зберегти пам'ять, типово буде вибрано лише ті компоненти, котрі напевно "
"будуть потрібні. Для звичайного встановлення всі інші компоненти "
"встановлювача не є необхідні, але вам можуть знадобитися деякі з них, "
"особливо певні модулі ядра, тому ретельно перегляньте список та виберіть "
"потрібні вам компоненти."

#. Type: text
#. Description
#. (Progress bar) title displayed when loading udebs
#. TRANSLATORS : keep short
#. :sl1:
#: ../anna.templates:3001
msgid "Loading additional components"
msgstr "Завантаження додаткових компонентів"

#. Type: text
#. Description
#. (Progress bar)
#. TRANSLATORS : keep short
#. :sl1:
#: ../anna.templates:4001
msgid "Retrieving ${PACKAGE}"
msgstr "Отримання ${PACKAGE}"

#. Type: text
#. Description
#. (Progress bar) title displayed when configuring udebs
#. TRANSLATORS : keep short
#. :sl1:
#: ../anna.templates:5001
msgid "Configuring ${PACKAGE}"
msgstr "Налаштування ${PACKAGE}"

#. Type: error
#. Description
#. :sl2:
#: ../anna.templates:7001
msgid "Failed to load installer component"
msgstr "Не вдалося завантажити компонент встановлювача"

#. Type: error
#. Description
#. :sl2:
#: ../anna.templates:7001
msgid "Loading ${PACKAGE} failed for unknown reasons. Aborting."
msgstr "Не вдалося завантажити ${PACKAGE} з невідомої причини. Дію перервано."

#. Type: error
#. Description
#. :sl2:
#: ../anna.templates:8001
#, fuzzy
msgid "No kernel modules found"
msgstr "Диски не знайдені"

#. Type: error
#. Description
#. :sl2:
#: ../anna.templates:8001
msgid ""
"No kernel modules were found. This probably is due to a mismatch between the "
"kernel used by this version of the installer and the kernel version "
"available in the archive."
msgstr ""
"Модулі ядра не знайдені. Можливо, це сталося тому, що версія ядра, що "
"використовується цією версією встановлювача, не збігається із версією, "
"наявною в архіві."

#. Type: error
#. Description
#. :sl2:
#: ../anna.templates:8001
msgid ""
"You should make sure that your installation image is up-to-date, or - if "
"that's the case - try a different mirror, preferably deb.debian.org."
msgstr ""
