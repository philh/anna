# THIS FILE IS GENERATED AUTOMATICALLY FROM THE D-I PO MASTER FILES
# The master files can be found under packages/po/
#
# DO NOT MODIFY THIS FILE DIRECTLY: SUCH CHANGES WILL BE LOST
#
# translation of debian-installer_packages_po_sublevel1_is.po to Icelandic
# Icelandic messages for debian-installer.
# This file is distributed under the same license as debian-installer.
# Copyright (C) 2003 Software in the Public Interest, Inc.
#
# Copyright (C) 2010 Free Software Foundation
# Sveinn í Felli <sv1@fellsnet.is>, 2018, 2019.
#
# Translations from iso-codes:
# Copyright (C) 2002,2003, 2010, 2011, 2012 Free Software Foundation, Inc.
# Translations from KDE:
# Þórarinn Rúnar Einarsson <thori@mindspring.com>
# zorglubb <debian-user-icelandic@lists.debian.org>, 2008.
# Sveinn í Felli <sveinki@nett.is>, 2010.
# Alastair McKinstry, <mckinstry@computer.org>, 2002.
# Sveinn í Felli <sveinki@nett.is>, 2010, 2011, 2012, 2013.
# Alastair McKinstry <mckinstry@computer.org>, 2002.
msgid ""
msgstr ""
"Project-Id-Version: Icelandic (Debian Installer)\n"
"Report-Msgid-Bugs-To: anna@packages.debian.org\n"
"POT-Creation-Date: 2019-11-05 23:57+0100\n"
"PO-Revision-Date: 2019-11-21 11:04+0000\n"
"Last-Translator: Sveinn í Felli <sv1@fellsnet.is>\n"
"Language-Team: Icelandic <translation-team-is@lists.sourceforge.net>\n"
"Language: is\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"

#. Type: multiselect
#. Description
#. :sl2:
#. Type: multiselect
#. Description
#. :sl2:
#: ../anna.templates:1001 ../anna.templates:2001
msgid "Installer components to load:"
msgstr "Hlutar uppsetningarkerfisins sem hlaða á inn:"

#. Type: multiselect
#. Description
#. :sl2:
#: ../anna.templates:1001
msgid ""
"All components of the installer needed to complete the install will be "
"loaded automatically and are not listed here. Some other (optional) "
"installer components are shown below. They are probably not necessary, but "
"may be interesting to some users."
msgstr ""
"Öll þau gögn sem uppsetningin mun þarfnast, verður hlaðið sjálfkrafa og eru "
"ekki taldir upp. Sumir hlutar, valmöguleikar, eru taldir upp hér fyrir "
"neðan. Þeirra er hugsanlega ekki þörf, en gætu vakið áhuga sumra notenda."

#. Type: multiselect
#. Description
#. :sl2:
#. Type: multiselect
#. Description
#. :sl2:
#: ../anna.templates:1001 ../anna.templates:2001
msgid ""
"Note that if you select a component that requires others, those components "
"will also be loaded."
msgstr ""
"Athugaðu að ef hluti sem valinn er þarfnast annara hluta verður þeim hlaðið "
"inn líka."

#. Type: multiselect
#. Description
#. :sl2:
#: ../anna.templates:2001
msgid ""
"To save memory, only components that are certainly needed for an install are "
"selected by default. The other installer components are not all necessary "
"for a basic install, but you may need some of them, especially certain "
"kernel modules, so look through the list carefully and select the components "
"you need."
msgstr ""
"Til að spara minni eru einungis þeir hlutar uppsetningarkerfisins sem þörf "
"er á valdir. Aðrir hlutar eru ekki nauðsynlegir fyrir einfalda uppsetningu. "
"Hugsanlegt er samt að þú þarfnist einhverra þeirra, þá sérstaklega "
"kjarnaeininganna, þannig að ráðlagt er að fletta í gegnum listann og velja "
"þá hluta sem þörf er á."

#. Type: text
#. Description
#. (Progress bar) title displayed when loading udebs
#. TRANSLATORS : keep short
#. :sl1:
#: ../anna.templates:3001
msgid "Loading additional components"
msgstr "Hleð aukahlutum"

#. Type: text
#. Description
#. (Progress bar)
#. TRANSLATORS : keep short
#. :sl1:
#: ../anna.templates:4001
msgid "Retrieving ${PACKAGE}"
msgstr "Sæki ${PACKAGE}"

#. Type: text
#. Description
#. (Progress bar) title displayed when configuring udebs
#. TRANSLATORS : keep short
#. :sl1:
#: ../anna.templates:5001
msgid "Configuring ${PACKAGE}"
msgstr "Stilli ${PACKAGE}"

#. Type: error
#. Description
#. :sl2:
#: ../anna.templates:7001
msgid "Failed to load installer component"
msgstr "Tókst ekki að hlaða hluta uppsetningarkerfisins"

#. Type: error
#. Description
#. :sl2:
#: ../anna.templates:7001
msgid "Loading ${PACKAGE} failed for unknown reasons. Aborting."
msgstr "Ekki tókst að hlaða inn ${PACKAGE} af einhverjum ástæðum. Hætti við."

#. Type: error
#. Description
#. :sl2:
#: ../anna.templates:8001
msgid "No kernel modules found"
msgstr "Engar kjarnaeiningar fundust"

#. Type: error
#. Description
#. :sl2:
#: ../anna.templates:8001
msgid ""
"No kernel modules were found. This probably is due to a mismatch between the "
"kernel used by this version of the installer and the kernel version "
"available in the archive."
msgstr ""
"Engar kjarnaeiningar fundust. Þetta er líklega vegna mismunar í útgáfu "
"kjarnans sem notaður er af þessari útgáfu uppsetningarkerfisins og "
"kjarnaútgáfunnar sem er í boði."

#. Type: error
#. Description
#. :sl2:
#: ../anna.templates:8001
msgid ""
"You should make sure that your installation image is up-to-date, or - if "
"that's the case - try a different mirror, preferably deb.debian.org."
msgstr ""
"Þú ættir að ganga úr skugga um að uppsetningarmiðillinn þinn sé ekki "
"úreltur, eða - ef sú er raunin - prófa annan spegil, helst deb.debian.org."
